﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GM : MonoBehaviour {

	public GameObject shark;
	GameObject shark_cn;
	public float pass_time;
	int bron_check = 0;
	public int standby_fish = 8;
	public float shark_show = 60;
	public float stay_time = 120;
	public float total_sum = 30;

	public GameObject standyby_ob_1;
	public GameObject standyby_ob_2;
	public GameObject standyby_ob_3;
	public GameObject standyby_ob_4;
	public GameObject standyby_ob_5;
	public GameObject standyby_ob_6;
	public GameObject standyby_ob_7;
	public GameObject standyby_ob_8;
	public GameObject standyby_ob_9;
	public GameObject standyby_ob_10;
	public GameObject standyby_ob_11;
	public GameObject standyby_ob_12;

	public GameObject setting_page;
	public InputField shark_show_txt;
	public InputField stay_time_txt;
	public InputField total_sum_txt;

//	int width = 800;
//	int high = 600;

//	public Text width_txt;
//	public Text high_txt;
    public InputField camdis_txt;
    float camdis = 76.3f;
    public Camera cam;

    public List<GameObject> line = new List<GameObject> ();
    public GameObject[] obs;
    public GameObject[] stuff;

    public GameObject target;
    public GameObject trackingpoint;

    // Use this for initialization
    void Start () 
	{
        /*shark_show = PlayerPrefs.GetFloat ("shark_show");
		stay_time = PlayerPrefs.GetFloat ("stay_time");
		total_sum = PlayerPrefs.GetFloat ("total_sum");
        camdis = PlayerPrefs.GetFloat("camdis");*/

        camdis_txt.text = camdis.ToString();
        shark_show_txt.text = shark_show.ToString();
        stay_time_txt.text = stay_time.ToString();
        total_sum_txt.text = total_sum.ToString();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (setting_page.activeSelf == false) 
			{
				setting_page.SetActive (true);
				return;
			} 
			else 
			{
				setting_page.SetActive (false);
			}
		}

		obs = GameObject.FindGameObjectsWithTag ("fishs");
        stuff = GameObject.FindGameObjectsWithTag("stuff");
        if (pass_time > 0) 
		{
			if (obs.Length < total_sum && line.Count != 0 && bron_check == 0)
			{
				StartCoroutine (wait_bron ());
			}
			else if (obs.Length < standby_fish && bron_check == 0) 
			{
				StartCoroutine (standy_bron ());
			}
		}
        if (pass_time >= shark_show)
        {
            for (int x = 0; x < obs.Length; x++)
            {
                if (obs[x].name == "trackingpoint(Clone)")
                {
                    obs[x].GetComponent<fishsc>().shark = true;
                }
                else
                {
                    obs[x].GetComponent<fishsc_1>().shark = true;
                }
            }

            shark_cn = Instantiate (shark);
			Destroy (shark_cn, 6f);
			pass_time = 0;
		} 
		if (shark_cn == null)
		{
			pass_time = pass_time + 1f * Time.deltaTime;
		}
	}
	private IEnumerator standy_bron ()
	{
		random_bron (Random.Range(1, 13));
        bron_check = 1;
		yield return new WaitForSeconds (4f);
		bron_check = 0;
	}
	private IEnumerator wait_bron ()
	{
		line [0].SetActive (true);
		line.RemoveAt (0);
		bron_check = 1;
		yield return new WaitForSeconds (4f);
		bron_check = 0;
	}

    GameObject standby_cn;

    void random_bron (int xx)
	{
		if (xx == 1)
		{
			standby_cn = Instantiate (standyby_ob_1);
            fish_setting(standby_cn);
        }
		if (xx == 2)
		{
			standby_cn = Instantiate (standyby_ob_2);
            fish_setting(standby_cn);
        }
		if (xx == 3)
		{
			standby_cn = Instantiate (standyby_ob_3);
            fish_setting(standby_cn);
        }
		if (xx == 4)
		{
			standby_cn = Instantiate (standyby_ob_4);
            fish_setting(standby_cn);
        }
		if (xx == 5)
		{
			standby_cn = Instantiate (standyby_ob_5);
            fish_setting(standby_cn);
        }
		if (xx == 6)
		{
			standby_cn = Instantiate (standyby_ob_6);
            fish_setting(standby_cn);
        }
		if (xx == 7)
		{
			standby_cn = Instantiate (standyby_ob_7);
            fish_setting(standby_cn);
        }
		if (xx == 8)
		{
			standby_cn = Instantiate (standyby_ob_8);
        }
		if (xx == 9)
		{
			standby_cn = Instantiate (standyby_ob_9);
        }
		if (xx == 10)
		{
			standby_cn = Instantiate (standyby_ob_10);
        }
		if (xx == 11)
		{
			standby_cn = Instantiate (standyby_ob_11);
        }
		if (xx == 12)
		{
			standby_cn = Instantiate (standyby_ob_12);
        }
	}
	public void setting_fn () 
	{
		shark_show = int.Parse (shark_show_txt.text);
		stay_time = int.Parse (stay_time_txt.text);
		total_sum = int.Parse (total_sum_txt.text);

        camdis = float.Parse(camdis_txt.text);
        cam.orthographicSize = camdis;
        
        

        //width = int.Parse (width_txt.text);
        //high = int.Parse (high_txt.text);

        //Screen.SetResolution (width,high,false);

        PlayerPrefs.SetFloat ("shark_show",shark_show);
		PlayerPrefs.SetFloat ("stay_time",stay_time);
		PlayerPrefs.SetFloat ("total_sum",total_sum);
        PlayerPrefs.SetFloat("camdis", camdis);

        setting_page.SetActive(false);
    }
	public void fullsc_fn ()
	{
        Application.Quit();
		//Screen.fullScreen = true;
		//setting_page.SetActive (false);
	}
	public void clear_all ()
	{
		for(int x = 0;x<obs.Length;x++)
		{
			Destroy (obs[x]);
		}
        for (int x = 0; x < stuff.Length; x++)
        {
            Destroy(stuff[x]);
        }
    }
    public void fish_setting(GameObject xx)
    {
        GameObject target_cn = Instantiate(target);
        GameObject trackingpoint_cn = Instantiate(trackingpoint);
        trackingpoint_cn.GetComponent<ranswim>().Target = target_cn;
        trackingpoint_cn.GetComponent<ranswim>().fish = xx;
        trackingpoint_cn.GetComponent<fishsc>().stay_time = stay_time;
        trackingpoint_cn.transform.localPosition = new Vector3(trackingpoint_cn.transform.localPosition.x, Random.RandomRange(-40f, 90f), trackingpoint_cn.transform.localPosition.z);
        //trackingpoint_cn.transform.localPosition = new Vector3(trackingpoint_cn.transform.localPosition.x, Random.RandomRange(-15f, 50f), trackingpoint_cn.transform.localPosition.z);
    }
}
