﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishsc : MonoBehaviour {

	public bool shark;
	public float stay_time = 120;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		stay_time = stay_time - 1f * Time.deltaTime;
		if (shark)
		{
            if (this.tag == "fishs")
            {
                this.GetComponent<ranswim>().fish.transform.localEulerAngles = new Vector3(0, 0, 0);
                this.GetComponent<ranswim>().fish.transform.Translate(Vector2.right * 2f);
                this.GetComponent<ranswim>().enabled = false;
                Destroy(this.GetComponent<ranswim>().Target, 5f);
                Destroy(this.GetComponent<ranswim>().fish, 5f);
                Destroy(this.gameObject, 5f);
            }
		}
		if (stay_time <= 0)
		{
            if (this.transform.localPosition.x <= -160 || this.transform.localPosition.x >= 160)
            {
                Destroy(this.GetComponent<ranswim>().Target);
                Destroy(this.GetComponent<ranswim>().fish);
                Destroy(this.gameObject);
            }
		}
	}
}
