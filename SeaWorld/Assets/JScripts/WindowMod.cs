﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowMod : MonoBehaviour
{
    public InputField ScrX;
    public InputField ScrY;
    public InputField ScrW;
    public InputField ScrH;

    public Rect screenPosition;
    [DllImport("user32.dll")]
    static extern IntPtr SetWindowLong(IntPtr hwnd, int _nIndex, int dwNewLong);
    [DllImport("user32.dll")]
    static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();
    const uint SWP_SHOWWINDOW = 0x0040;
    const int GWL_STYLE = -16;
    const int WS_BORDER = 1;

    //public GameObject bg;

    void Start()
    {
        /*screenPosition.width = PlayerPrefs.GetFloat("width");
        screenPosition.height = PlayerPrefs.GetFloat("high");
        screenPosition.x = PlayerPrefs.GetFloat("scrx");
        screenPosition.y = PlayerPrefs.GetFloat("scry");
        ScrX.text = screenPosition.x.ToString();
        ScrY.text = screenPosition.y.ToString();
        ScrW.text = screenPosition.width.ToString();
        ScrH.text = screenPosition.height.ToString();

        SetWindowLong(GetForegroundWindow(), GWL_STYLE, WS_BORDER);
        bool result = SetWindowPos(GetForegroundWindow(), 0, (int)screenPosition.x, (int)screenPosition.y, (int)screenPosition.width, (int)screenPosition.height, SWP_SHOWWINDOW);
        */
    }

        public void change_de()
    {
        screenPosition.width = int.Parse(ScrW.text);
        screenPosition.height = int.Parse(ScrH.text);
        screenPosition.x = int.Parse(ScrX.text);
        screenPosition.y = int.Parse(ScrY.text);

        //bg.transform.localScale = new Vector3(screenPosition.width, screenPosition.height, 1);

        SetWindowLong(GetForegroundWindow(), GWL_STYLE, WS_BORDER);
        bool result = SetWindowPos(GetForegroundWindow(), 0, (int)screenPosition.x, (int)screenPosition.y, (int)screenPosition.width, (int)screenPosition.height, SWP_SHOWWINDOW);

        PlayerPrefs.SetFloat("width", screenPosition.width);
        PlayerPrefs.SetFloat("high", screenPosition.height);
        PlayerPrefs.SetFloat("scrx", screenPosition.x);
        PlayerPrefs.SetFloat("scry", screenPosition.y);
    }
}
