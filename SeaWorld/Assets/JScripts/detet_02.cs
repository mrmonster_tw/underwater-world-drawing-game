﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class detet_02 : MonoBehaviour {

	public string file_02;
	string file_03;
	string file_04;
	string file_05;
	string file_06;
	string file_07;
	string file_08;
	string file_09;
	string file_10;
	string file_11;
	string file_12;
	string file_13;
	private string nam;
	public string matnam;
	bool born_02;
	bool born_03;
	bool born_04;
	bool born_05;
	bool born_06;
	bool born_07;
	bool born_08;
	bool born_09;
	bool born_10;
	bool born_11;
	bool born_12;
	bool born_13;

	Material mat;
	SkinnedMeshRenderer[] obj_m;
	TextMesh[] obj_t;
	public Material test;
	public RenderTexture qrcam;

	public GameObject model_02;
	public GameObject model_03;
	public GameObject model_04;
	public GameObject model_05;
	public GameObject model_06;
	public GameObject model_07;
	public GameObject model_08;
	public GameObject model_09;
	public GameObject model_10;
	public GameObject model_11;
	public GameObject model_12;
	public GameObject model_13;
	public GameObject model_cn;

	public Text place;

	FileSystemWatcher watch_02;
	FileSystemWatcher watch_03;
	FileSystemWatcher watch_04;
	FileSystemWatcher watch_05;
	FileSystemWatcher watch_06;
	FileSystemWatcher watch_07;
	FileSystemWatcher watch_08;
	FileSystemWatcher watch_09;
	FileSystemWatcher watch_10;
	FileSystemWatcher watch_11;
	FileSystemWatcher watch_12;
	FileSystemWatcher watch_13;

    // Use this for initialization
    void Start () 
	{
		change_path ();
	}
	public void close_listen()
	{
		//關閉監聽
		print("aa");
		watch_02.EnableRaisingEvents = false;
		watch_03.EnableRaisingEvents = false;
		watch_04.EnableRaisingEvents = false;
		watch_05.EnableRaisingEvents = false;
		watch_06.EnableRaisingEvents = false;
		watch_07.EnableRaisingEvents = false;
		watch_08.EnableRaisingEvents = false;
		watch_09.EnableRaisingEvents = false;
		watch_10.EnableRaisingEvents = false;
		watch_11.EnableRaisingEvents = false;
		watch_12.EnableRaisingEvents = false;
		watch_13.EnableRaisingEvents = false;
	}
	public void change_path()
	{
		file_02 = @"C:\Scan\"+place.text+"02"; //針測的資料夾
		file_03 = @"C:\Scan\"+place.text+"03"; //針測的資料夾
		file_04 = @"C:\Scan\"+place.text+"04"; //針測的資料夾
		file_05 = @"C:\Scan\"+place.text+"05"; //針測的資料夾
		file_06 = @"C:\Scan\"+place.text+"06"; //針測的資料夾
		file_07 = @"C:\Scan\"+place.text+"07"; //針測的資料夾
		file_08 = @"C:\Scan\"+place.text+"08"; //針測的資料夾
		file_09 = @"C:\Scan\"+place.text+"09"; //針測的資料夾
		file_10 = @"C:\Scan\"+place.text+"10"; //針測的資料夾
		file_11 = @"C:\Scan\"+place.text+"11"; //針測的資料夾
		file_12 = @"C:\Scan\"+place.text+"12"; //針測的資料夾
		file_13 = @"C:\Scan\"+place.text+"13"; //針測的資料夾
		//設定針測的檔案類型，如不指定可設定*.*
		watch_02 = new FileSystemWatcher(file_02, "*.jpg");
		watch_03 = new FileSystemWatcher(file_03, "*.jpg");
		watch_04 = new FileSystemWatcher(file_04, "*.jpg");
		watch_05 = new FileSystemWatcher(file_05, "*.jpg");
		watch_06 = new FileSystemWatcher(file_06, "*.jpg");
		watch_07 = new FileSystemWatcher(file_07, "*.jpg");
		watch_08 = new FileSystemWatcher(file_08, "*.jpg");
		watch_09 = new FileSystemWatcher(file_09, "*.jpg");
		watch_10 = new FileSystemWatcher(file_10, "*.jpg");
		watch_11 = new FileSystemWatcher(file_11, "*.jpg");
		watch_12 = new FileSystemWatcher(file_12, "*.jpg");
		watch_13 = new FileSystemWatcher(file_13, "*.jpg");
		//開啟監聽
		watch_02.EnableRaisingEvents = true;
		watch_03.EnableRaisingEvents = true;
		watch_04.EnableRaisingEvents = true;
		watch_05.EnableRaisingEvents = true;
		watch_06.EnableRaisingEvents = true;
		watch_07.EnableRaisingEvents = true;
		watch_08.EnableRaisingEvents = true;
		watch_09.EnableRaisingEvents = true;
		watch_10.EnableRaisingEvents = true;
		watch_11.EnableRaisingEvents = true;
		watch_12.EnableRaisingEvents = true;
		watch_13.EnableRaisingEvents = true;

		//新增時觸發事件
		watch_02.Created += listen_02;
		watch_03.Created += listen_03;
		watch_04.Created += listen_04;
		watch_05.Created += listen_05;
		watch_06.Created += listen_06;
		watch_07.Created += listen_07;
		watch_08.Created += listen_08;
		watch_09.Created += listen_09;
		watch_10.Created += listen_10;
		watch_11.Created += listen_11;
		watch_12.Created += listen_12;
		watch_13.Created += listen_13;
	}
	void listen_02(object sender, FileSystemEventArgs e)
	{
		born_02 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_03(object sender, FileSystemEventArgs e)
	{
		born_03 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_04(object sender, FileSystemEventArgs e)
	{
		born_04 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_05(object sender, FileSystemEventArgs e)
	{
		born_05 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_06(object sender, FileSystemEventArgs e)
	{
		born_06 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_07(object sender, FileSystemEventArgs e)
	{
		born_07 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_08(object sender, FileSystemEventArgs e)
	{
		born_08 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_09(object sender, FileSystemEventArgs e)
	{
		born_09 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_10(object sender, FileSystemEventArgs e)
	{
		born_10 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_11(object sender, FileSystemEventArgs e)
	{
		born_11 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_12(object sender, FileSystemEventArgs e)
	{
		born_12 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	void listen_13(object sender, FileSystemEventArgs e)
	{
		born_13 = true;
		nam = e.Name;
		matnam = Path.GetFileNameWithoutExtension(e.Name);
	}
	
	// Update is called once per frame
	void Update ()
	{
		bron02_fn ();
		bron03_fn ();
		bron04_fn ();
		bron05_fn ();
		bron06_fn ();
		bron07_fn ();
		bron08_fn ();
		bron09_fn ();
		bron10_fn ();
		bron11_fn ();
		bron12_fn ();
		bron13_fn ();
	}
	private void bron02_fn ()
	{
		if (born_02)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_02);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_02 = false;
		}
	}
	private void bron03_fn ()
	{
		if (born_03)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_03);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_03 = false;
		}
	}
	private void bron04_fn ()
	{
		if (born_04)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_04);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

            model_cn.GetComponent<fishsc_1>().stay_time = this.GetComponent<GM>().stay_time;
            this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_04 = false;
		}
	}
	private void bron05_fn ()
	{
		if (born_05)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_05);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;


			model_cn.GetComponent<fishsc_1> ().stay_time = this.GetComponent<GM> ().stay_time;
			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_05 = false;
		}
	}
	private void bron06_fn ()
	{
		if (born_06)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_06);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_06 = false;
		}
	}
	private void bron07_fn ()
	{
		if (born_07)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_07);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

            model_cn.GetComponent<fishsc_1>().stay_time = this.GetComponent<GM>().stay_time;
            this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_07 = false;
		}
	}
	private void bron08_fn ()
	{
		if (born_08)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_08);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_08 = false;
		}
	}
	private void bron09_fn ()
	{
		if (born_09)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_09);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_09 = false;
		}
	}
	private void bron10_fn ()
	{
		if (born_10)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_10);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

            model_cn.GetComponent<fishsc_1>().stay_time = this.GetComponent<GM>().stay_time;
            this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_10 = false;
		}
	}
	private void bron11_fn ()
	{
		if (born_11)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_11);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_11 = false;
		}
	}
	private void bron12_fn ()
	{
		if (born_12)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_12);
            this.GetComponent<GM>().fish_setting(model_cn);

            obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;

			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_12 = false;
		}
	}
	private void bron13_fn ()
	{
		if (born_13)
		{
			//创建文件读取流
			FileStream fs = new FileStream(nam, FileMode.Open, FileAccess.Read);
			fs.Seek(0, SeekOrigin.Begin);
			//创建文件长度缓冲区
			byte[] bytes = new byte[fs.Length];
			//读取文件
			fs.Read(bytes, 0, (int)fs.Length);
			//释放文件读取流
			fs.Close();
			fs.Dispose();
			fs = null;

			//创建材质，并设置默认着色器 
			//mat[count] = new Material(Shader.Find("Transparent/Diffuse"));
			mat = new Material(test);
			//加载图片资源
			Texture2D texture = new Texture2D(2048, 2048);
			texture.LoadImage(bytes);
			//为材质加载图片  
			mat.mainTexture = texture;

			model_cn = Instantiate (model_13);
			obj_m = model_cn.GetComponentsInChildren<SkinnedMeshRenderer>();
			obj_m[0].material = mat;
			obj_t = model_cn.GetComponentsInChildren<TextMesh> ();
			obj_t[0].text = matnam;
			obj_t[1].text = matnam;


			model_cn.GetComponent<fishsc_1> ().stay_time = this.GetComponent<GM> ().stay_time;
			this.GetComponent<GM> ().line.Add (model_cn);
			model_cn.SetActive (false);

			born_13 = false;
		}
	}
}
