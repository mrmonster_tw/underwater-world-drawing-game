﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walk_ran_ani : MonoBehaviour {

	Animator ani;

	// Use this for initialization
	void Start () 
	{
		ani = GetComponent<Animator> ();
		random_ani (Random.Range (0,2));
    }
	
	// Update is called once per frame
	void Update () 
	{
        
	}
	void random_ani (int xx) 
	{
		if (xx == 0)
		{
			ani.Play ("walk");
		}
		if (xx == 1)
		{
			ani.Play ("walk_1");
		}
	}
    
}
