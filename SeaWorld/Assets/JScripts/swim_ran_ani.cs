﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swim_ran_ani : MonoBehaviour {

	Animator ani;

	// Use this for initialization
	void Start () 
	{
		ani = GetComponent<Animator> ();
		random_ani (Random.Range (0,4));
	}
	
	// Update is called once per frame
	void Update () 
	{
			
	}
	void random_ani (int xx) 
	{
		if (xx == 0)
		{
			ani.Play ("newswim_1");
		}
		if (xx == 1)
		{
			ani.Play ("newswim_2");
		}
		if (xx == 2)
		{
			ani.Play ("newswim_3");
		}
        if (xx == 3)
        {
            ani.Play("newswim_4");
        }
    }
}
