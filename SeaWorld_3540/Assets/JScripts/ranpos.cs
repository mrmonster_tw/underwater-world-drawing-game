﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ranpos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        random_pos(Random.Range(0, 5));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void random_pos(int xx)
    {
        if (xx == 0)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -3);
        }
        if (xx == 1)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -6);
        }
        if (xx == 2)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -9);
        }
        if (xx == 3)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -12);
        }
        if (xx == 4)
        {
            this.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -15);
        }
    }
}
