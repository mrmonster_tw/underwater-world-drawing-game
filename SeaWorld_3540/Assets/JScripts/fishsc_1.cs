﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishsc_1 : MonoBehaviour
{
    public bool shark;
    public float stay_time = 120;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        stay_time = stay_time - 1f * Time.deltaTime;
        if (shark)
        {
            if (this.tag == "fishs")
            {
                this.GetComponent<Animator>().enabled = false;
                this.transform.localEulerAngles = new Vector3(0, 0, 0);
                this.transform.Translate(Vector2.right * 2f);
                Destroy(this.gameObject, 5f);
            }
        }
        if (stay_time <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
