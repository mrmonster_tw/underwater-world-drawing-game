﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ranswim : MonoBehaviour
{
    public bool shark = false;
    public float speedcore = 15;
    public float speed = 10;
    public float trunspeed = 0.01f;

    Vector3 newDir;
    Vector3 targetDir;
    public GameObject Target;

    public GameObject fish;
    float dis;
    float XZ = 0;

    int rn;

    void Start()
    {
        ran_weidth();
        //if (this.transform.position.x == 160)
        if (this.transform.position.x == 230)
        {
            //XZ = -160;
            XZ = -230;
        }
        //if (this.transform.position.x == -160)
        if (this.transform.position.x == -230)
        {
            //XZ = 160;
            XZ = 230;
        }
        rn = Random.Range(0,2);
        if (rn == 0)
        {

        }
    }

    // Update is called once per frame
    void Update()
    {
        targetDir = Target.transform.position - transform.position;
        dis = Vector3.Distance(Target.transform.position, transform.position);

        newDir = Vector3.RotateTowards(transform.forward, targetDir, trunspeed, 0.0F);
        transform.rotation = Quaternion.LookRotation(newDir);
        transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);

        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y,-20);


        /*if (transform.position.x <= -160 && Target.transform.position.x == -160)
        {
            XZ = 160;
            fish.transform.eulerAngles = new Vector3(0, 0, 0);
        }
        if (transform.position.x >= 160 && Target.transform.position.x == 160)
        {
            XZ = -160;
            fish.transform.eulerAngles = new Vector3(0,180,0);
        }*/
        if (transform.position.x <= -230)
        {
            XZ = 230;
            if (Target.transform.position.x == -230)
            {
                fish.transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }
        if (transform.position.x >= 230)
        {
            XZ = -230;
            if (Target.transform.position.x == 230)
            {
                fish.transform.eulerAngles = new Vector3(0, 180, 0);
            }
        }
        speed = Mathf.Lerp(speed, speedcore,0.01f);

        fish.transform.position = this.transform.position;
    }
    void ran_weidth()
    {
        StartCoroutine(hold_t());
    }
    private IEnumerator hold_t()
    {
        yield return new WaitForSeconds(Random.RandomRange(5f, 8f));
        //Target.transform.position = new Vector3(XZ, Random.RandomRange(-40f, 90f), -20);
        Target.transform.position = new Vector3(XZ, Random.RandomRange(-15f, 50f), -20);
        speedcore = Random.RandomRange(0,25);
        ran_weidth();
    }
}
